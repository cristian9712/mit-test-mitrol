package com.mitrol.example.ticketAdmin.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitrol.example.ticketAdmin.entity.Ticket;
import com.mitrol.example.ticketAdmin.model.TicketStatus;

public interface TicketRepository extends JpaRepository<Ticket,Integer> {

    @Query("select ticket from Ticket ticket where ticket.status IN (:listState)")
    public List<Ticket> findTicketsByListState(@Param("listState") List<TicketStatus> states);
	
    public List<Ticket> findByStatus(TicketStatus status);
}
