package com.mitrol.example.ticketAdmin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitrol.example.ticketAdmin.entity.Ticket;
import com.mitrol.example.ticketAdmin.model.ErrorMessage;
import com.mitrol.example.ticketAdmin.model.TicketDto;
import com.mitrol.example.ticketAdmin.model.TicketStatus;
import com.mitrol.example.ticketAdmin.repository.TicketRepository;

@Service
public class TicketService {

	@Autowired
	private TicketRepository repository;

	private TicketDto toDto(Ticket entity) {
		TicketDto dto = new TicketDto();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setStatus(entity.getStatus());
		return dto;
	}

	private Ticket toEntity(TicketDto dto) {
		Ticket entity = new Ticket();
		entity.setName(dto.getName());
		entity.setStatus(dto.getStatus());
		return entity;
	}

	public List<TicketDto> findAll() {
		List<Ticket> entities = repository.findAll();
		return entities.stream().map(this::toDto).collect(Collectors.toList());
	}

	public TicketDto create(TicketDto ticket) {
		Ticket entity = toEntity(ticket);
		repository.save(entity);
		return toDto(entity);
	}

	/**
	 * Metodo encargado de buscar un ticket por id
	 *
	 * @author Cristian Contreras
	 * @throws Exception
	 * @since 2020-01-24
	 */
	public TicketDto findById(Integer id) throws Exception {
		Ticket entity = repository.findById(id).orElse(null);
		if (entity != null) {
			return toDto(entity);
		} else {
			throw new Exception(ErrorMessage.TICKET_NOT_EXIST.getMessage());
		}
	}

	/**
	 * Metodo encargado de actualizar un ticket por id
	 *
	 * @author Cristian Contreras
	 * @throws Exception
	 * @since 2020-01-24
	 */
	public TicketDto update(Integer id, TicketDto ticket) throws Exception {
		Ticket entityBD = repository.findById(id).orElse(null);
		if (entityBD != null) {
			Ticket entity = toEntity(ticket);
			entity.setId(id);
			repository.save(entity);
			return toDto(entity);
		} else {
			throw new Exception(ErrorMessage.TICKET_TO_UPDATE_NOT_EXIST.getMessage());
		}
	}

	/**
	 * Metodo encargado de borrar un ticket por id
	 *
	 * @author Cristian Contreras
	 * @throws Exception 
	 * @since 2020-01-24
	 */
	public void delete(Integer id) throws Exception {
		Ticket entityBD = repository.findById(id).orElse(null);
		if (entityBD != null) {
			if(entityBD.getStatus() == TicketStatus.DONE) {
				repository.delete(entityBD);
			}else {
				throw new Exception(ErrorMessage.TICKET_STATUS_IS_NOT_DONE_IT_IS_NOT_POSSIBLE_TO_DELETE.getMessage());
			}
		}else {
			throw new Exception(ErrorMessage.TICKET_TO_DELETE_NOT_EXIST.getMessage());
		}
	}

	/**
	 * Metodo encargado de buscar los tickets que aun no estan en estado DONE o
	 * finalizado
	 *
	 * @author Cristian Contreras
	 * @since 2020-01-24
	 */
	public List<TicketDto> findAllTicketsNotFinifhed() {
		List<TicketStatus> listStatus = new ArrayList<>();
		listStatus.add(TicketStatus.PENDING);
		listStatus.add(TicketStatus.WORKING);
		List<Ticket> entities = repository.findTicketsByListState(listStatus);
		return entities.stream().map(this::toDto).collect(Collectors.toList());
	}
}
