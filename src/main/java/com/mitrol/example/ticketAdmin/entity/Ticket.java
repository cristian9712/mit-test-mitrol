package com.mitrol.example.ticketAdmin.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.mitrol.example.ticketAdmin.model.TicketStatus;

@Entity
public class Ticket {

	@GeneratedValue
	@Id
	private Integer id;

	private String name;

	private TicketStatus status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TicketStatus getStatus() {
		return status;
	}

	public void setStatus(TicketStatus status) {
		this.status = status;
	}
}
