package com.mitrol.example.ticketAdmin.model;

public enum ErrorMessage {

	TICKET_NOT_EXIST("El ticket no existe"), 
	TICKET_TO_UPDATE_NOT_EXIST("El ticket a actualizar no existe"), 
	TICKET_TO_DELETE_NOT_EXIST("El ticket a eliminar no existe"), 
	TICKET_STATUS_IS_NOT_DONE_IT_IS_NOT_POSSIBLE_TO_DELETE("El ticket aun no se ha finalizado, aun no lo puede eliminar");
 
    private String message;
 
    ErrorMessage(String message) {
        this.message = message;
    }
 
    public String getMessage() {
        return message;
    }
}
