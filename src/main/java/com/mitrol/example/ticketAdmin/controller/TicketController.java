package com.mitrol.example.ticketAdmin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mitrol.example.ticketAdmin.model.ApiError;
import com.mitrol.example.ticketAdmin.model.TicketDto;
import com.mitrol.example.ticketAdmin.service.TicketService;

@RestController
public class TicketController {

	@Autowired
	private TicketService service;

	@GetMapping("/ticket")
	public List<TicketDto> findAll() {
		return service.findAll();
	}

	@PostMapping("/ticket")
	public TicketDto create(@RequestBody TicketDto ticket) {
		return service.create(ticket);
	}

	/**
	 * Metodo encargado de buscar un ticket por id
	 *
	 * @author Cristian Contreras
	 * @since 2020-01-24
	 */
	@GetMapping("/ticket/{id}")
	public ResponseEntity<Object> findById(@PathVariable("id") Integer id) {
		try {
			TicketDto ticketDto = service.findById(id);
			return ResponseEntity.ok().body(ticketDto);
		}catch(Exception e) {
			ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, e.getMessage(), e.getMessage());
			return ResponseEntity.badRequest().body(apiError);
		}
		
	}

	/**
	 * Metodo encargado de actualizar un ticket por id
	 *
	 * @author Cristian Contreras
	 * @since 2020-01-24
	 */
	@PutMapping("/ticket/{id}")
	public ResponseEntity<Object> update(@PathVariable("id") Integer id, @RequestBody TicketDto ticket) {
		try {
			TicketDto ticketDto = service.update(id, ticket);
			return ResponseEntity.ok().body(ticketDto);
		}catch(Exception e) {
			ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, e.getMessage(), e.getMessage());
			return ResponseEntity.badRequest().body(apiError);
		}
	}

	/**
	 * Metodo encargado de borrar un ticket por id
	 *
	 * @author Cristian Contreras
	 * @since 2020-01-24
	 */
	@DeleteMapping("/ticket/{id}")
	public ResponseEntity<Object>  delete(@PathVariable("id") Integer id) {
		try {
			service.delete(id);
			return ResponseEntity.ok().build();
		}catch(Exception e) {
			ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, e.getMessage(), e.getMessage());
			return ResponseEntity.badRequest().body(apiError);
		}
		
	}

	/**
	 * Metodo encargado de buscar los tickets que aun no estan en estado DONE o
	 * finalizado
	 *
	 * @author Cristian Contreras
	 * @since 2020-01-24
	 */
	@GetMapping("/ticket/notFinished")
	public List<TicketDto> findAllTicketsNotFinifhed() {
		return service.findAllTicketsNotFinifhed();
	}

}
