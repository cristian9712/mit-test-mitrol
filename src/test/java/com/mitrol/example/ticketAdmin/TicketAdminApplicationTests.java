package com.mitrol.example.ticketAdmin;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrol.example.ticketAdmin.model.ErrorMessage;
import com.mitrol.example.ticketAdmin.model.TicketDto;
import com.mitrol.example.ticketAdmin.model.TicketStatus;


@SpringBootTest(classes = TicketAdminApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
		locations = "classpath:application-integrationtest.properties")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class TicketAdminApplicationTests {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	WebApplicationContext webApplicationContext;

	protected String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}
	
	@SuppressWarnings("unchecked")
	protected <T> T mapToObject(String json, Class<T> clazz) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return (T) objectMapper.readValue(json, clazz);
	}

	@Test
	@Order(1)
	void fetchTicketTest() throws Exception {
		mvc.perform(get("/ticket")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$", is(Matchers.empty())));
	}


	@Test
	@Order(2)
	void createTicketTest() throws Exception {
		String ticketNameTest = "Test 1";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);

		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));

		mvc.perform(get("/ticket")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].name", is(ticketNameTest)));

	}
	
	@Test
	@Order(3)
	void findTicketByIdTest() throws Exception {
		String ticketNameTest = "Test 2";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);

		MvcResult resultCreate = mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)))
				.andReturn();
		
		String jsonRespCreate = resultCreate.getResponse().getContentAsString();
		TicketDto ticketResponse = mapToObject(jsonRespCreate, TicketDto.class);
		
		mvc.perform(get("/ticket/" + ticketResponse.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));

	}
	
	@Test
	@Order(4)
	void updateTicketTest() throws Exception {
		String ticketNameTest = "Test 3";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);

		MvcResult resultCreate = mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)))
				.andReturn();
		
		String jsonRespCreate = resultCreate.getResponse().getContentAsString();
		TicketDto ticketResponse = mapToObject(jsonRespCreate, TicketDto.class);

		ticket.setStatus(TicketStatus.WORKING);
		
		mvc.perform(put("/ticket/" + ticketResponse.getId())
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.status", is(ticket.getStatus().name())));
		
		ticket.setStatus(TicketStatus.DONE);
		
		mvc.perform(put("/ticket/" + ticketResponse.getId())
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.status", is(ticket.getStatus().name())));
	}
	
	@Test
	@Order(5)
	void deleteTicketTest() throws Exception {
		String ticketNameTest = "Test 4";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);
		ticket.setStatus(TicketStatus.DONE);

		MvcResult resultCreate = mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)))
				.andReturn();
		
		String jsonRespCreate = resultCreate.getResponse().getContentAsString();
		TicketDto ticketResponse = mapToObject(jsonRespCreate, TicketDto.class);

		mvc.perform(get("/ticket/" + ticketResponse.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));
	}
	
	@Test
	@Order(6)
	void noFinishedTicketsTest() throws Exception {
		String ticketNameTest = "Test 5";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);
		ticket.setStatus(TicketStatus.PENDING);

		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));
		
		
		ticket.setStatus(TicketStatus.WORKING);
		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));
		
		
		ticket.setStatus(TicketStatus.DONE);
		mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)));

		mvc.perform(get("/ticket/notFinished")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
//				.andExpect(jsonPath("$[*].status", is(Matchers.contains(Matchers.anyOf(containsString("PENDING"),containsString("WORKING"))))));
	}

	
	
	//ESCENARIOS NO FELICES
	@Test
	@Order(7)
	void findTicketByIdFailTest() throws Exception {
		
		mvc.perform(get("/ticket/1000")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.errors", is(Matchers.contains(ErrorMessage.TICKET_NOT_EXIST.getMessage()))));
	}
	
	@Test
	@Order(8)
	void updateTicketFailTest() throws Exception {
		String ticketNameTest = "Test Fail Update";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);
		ticket.setStatus(TicketStatus.WORKING);
		
		mvc.perform(put("/ticket/1000")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.errors", is(Matchers.contains(ErrorMessage.TICKET_TO_UPDATE_NOT_EXIST.getMessage()))));
	}
	
	@Test
	@Order(9)
	void deleteTicketFailTest() throws Exception {
		
		mvc.perform(delete("/ticket/1000")
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.errors", is(Matchers.contains(ErrorMessage.TICKET_TO_DELETE_NOT_EXIST.getMessage()))));
	}
	
	@Test
	@Order(9)
	void deleteTicketNotDoneFailTest() throws Exception {
		
		String ticketNameTest = "Test 4";
		TicketDto ticket = new TicketDto();
		ticket.setName(ticketNameTest);
		ticket.setStatus(TicketStatus.PENDING);

		MvcResult resultCreate = mvc.perform(post("/ticket")
				.content(mapToJson(ticket))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.name", is(ticketNameTest)))
				.andReturn();
		
		String jsonRespCreate = resultCreate.getResponse().getContentAsString();
		TicketDto ticketResponse = mapToObject(jsonRespCreate, TicketDto.class);
		
		mvc.perform(delete("/ticket/" + ticketResponse.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isBadRequest())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.errors", is(Matchers.contains(ErrorMessage.TICKET_STATUS_IS_NOT_DONE_IT_IS_NOT_POSSIBLE_TO_DELETE.getMessage()))));
	}
}
